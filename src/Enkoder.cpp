/*****************************************************************************
 *         �WICZENIE 4  -  ENKODER         				  ver. 29.10.2020    *
 *																			 *
 *****************************************************************************/
#include 	<21369.h>
#include 	<def21369.h>
#include 	<cdef21369.h>
#include 	<signal.h>
#include 	<stdio.h>
#include 	<stdlib.h>
#include 	<math.h>
#include	<configdai.c>
#include 	<stdio.h> 
#include 	<string.h>
void timer_isr	(int sig);
void ADC_IRQ2	(int sig);
#include	"zmienne.h"		//definicje zmiennych i sta�ych uzywanych w programie
#include	"hardware.h"	//adresy ukladow peryferyjnych
#include	"setup.h"		//ustawienia konfiguracji DSP/FPGA
#include	"estymator.h"	//estymator strumienia maszyny ind.
#include 	"procedury.h"		
#include 	"timer.hpp"		//obs�uga przerwan timera
#include	"metody.h"
#include	"ADC.h"			//obs�uga przerwania od przetwornika

int main( int argc, char *argv[] )
{
 set_flag(SET_FLAG0, CLR_FLAG);
 set_flag(SET_FLAG1, CLR_FLAG); 
 set_flag(SET_FLAG3, CLR_FLAG);	
 setup(); 

 BUZZER (true);
 wait_us(50000);
 BUZZER (false);

 char LCD_L1[64];         //zmienna do przechowywania tekstu
 LCD_CLR();

 while(1)
 	{	
	//	LCD_POZYCJA(0x00);//" <---12---> "
		sprintf(&LCD_L1[0], "Frek= %1.2f  ",(P1*50));
		sprintf(&LCD_L1[12],"Isz= %1.2f   ",(abs(P1*2)));  
		sprintf(&LCD_L1[40],"ENC= %u      ",(ENC));     
		sprintf(&LCD_L1[52],"             ");
		LCD_CLR();
		LCD_PISZ_SLOWO (LCD_L1,64);
		wait_us(50000);		
	} 
}

//##################################################
