// ADC.h
// obs�uga przerwania od przetwornika


void ADC_IRQ2	(int sig)
	{	
	// ----------------- Odczyt z przetwornikow ADC -------------------------	
	ADC3_RD(ADC3);	//Odczyt z przetwornikow ADC
	UDC =-(float)ADC3.CH1*(10.0/8191)*12.45;	//Napiecie Udc [V] 
	Is.w=(float)ADC3.CH2*(10.0/8191)*1.6;		//prady sieci [A]
	Is.v=(float)ADC3.CH3*(10.0/8191)*1.6;		//
	Is.u=(float)ADC3.CH4*(10.0/8191)*1.6;
				
	ADC1_RD(ADC1);	
	P1=Normalizuj(ADC1.CH1,POT1);	// Potencjomet1 (+/-)1.0
	P2=Normalizuj(ADC1.CH2,POT2);	// Potencjomet2 (+/-)1.0
	P3=Normalizuj(ADC1.CH3,POT3);	// Potencjomet3 (+/-)1.0
	P4=Normalizuj(ADC1.CH4,POT4);	// Potencjomet4 (+/-)1.0
	// ----------------------------------------------------------------------
	
	ENC=*CS_ENC_KAT&0x0FFF;			// [0 - 4095]

	// DCAC_zad(freq, ampl, -> Isz.u Isz.v Isz.w)
	DCAC_zad(P1*50, P1*5, Isz);// Generator zadanych wartosci sin.  

	// Dmod2_DCAC(pr�d zad, pr�d mierz, histereza)
	stan_kluczy=Dmod2_DCAC(Isz, Is, 0.0);	//Sterowanie z reg. nieliniowymi typu delta
	*CS_DELTA_F=stan_kluczy;				//DSP -> FPGA -> falownik

KAT=(ENC*2*PI)/4095;
czas=czas+Tp;
if(czas>=4e-3)
{
kat2=kat1;
kat1=KAT;
delta=kat1-kat2;
speed=delta/czas;
czas=0;
}
		

// ------------------ Obsluga przetwornikow DAC -------------------------
		DAC.A= ENC*0.001;
		DAC.B= 0;
		DAC.C= 0;
		DAC.D= 0.0; 
		DAC.E= 0;
		DAC.F= Is.u;
		DAC.G= Is.v;
		DAC.H= Is.w;
		DAC_WR(DAC);		
// ----------------------------------------------------------------------
} 
 			