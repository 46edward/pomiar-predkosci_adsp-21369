#define	p3		1.732050807 //pierwiastek z 3
#define	p2		1.41421		//pierwiastek z 2
#define	d2n3	(2.0/3.0)
#define	d3n2	(3.0/2.0)
#define	jp3		(1.0/p3)
#define	jp2		(1.0/p2)
#define	p3n2		(p3/2.0) 	//pierwiastek3/2
#define	p3n3		(p3/3.0) 	//pierwiastek3/3
#define	PI		3.141592653
#define	dwaPI	(2.0*PI)
#define	Ldlawik	1.6e-3      //11.3e-3

struct	daneAC {volatile float CH1,CH2,CH3,CH4,CH5,CH6,CH7,CH8;};
struct   KalibPot{volatile float min,max,offset,norma;};
struct	daneCA {volatile float A,B,C,D,E,F,G,H;}; 

struct	wektor
	{
	volatile float	u,v,w;				//uklad 3-fazowy 
	volatile float	a,b;				//uklad wsp. alfa beta
	volatile float  d,q;			   	//uklad wsp. w ukl wirujacym (silnik)
	volatile float  x,y;				//uklad wsp. w ukl wirujacym (siec)
	volatile float	mod,kat,sink,cosk;	//modul wektora kat oraz sinus i cosinus konta
	};
		
//zmienne przetwornik�w DAC/ADC: =================================================
daneAC ADC1,ADC2,ADC3;
daneCA DAC;	
float	P1,P2,P3,P4;					//potjometry P1-P4	      											   
	KalibPot POT1={-1500,1500,0.0,1500.0}; //wartoci dobrane empirycznie
	KalibPot POT2={-1500,1500,0.0,1500.0};
	KalibPot POT3={-1500,1500,0.0,1500.0};
	KalibPot POT4={-1500,1500,0.0,1500.0};
//================================================================================

//zmienne przeksztaltnika DC/AC (generator): =====================================
wektor			PSI,Is,Isz,Us;	// wektory strumiena, pr�d�w i napi�� 
volatile int	sektor,ENC;		
volatile float	UDC,Me,Me_zad,dU,dV,dW,dM;
volatile float	wm=0.0,katm=0.0;
volatile int	kompU,kompV,kompW,kompM; 			   // komparatory
volatile int	stan_kluczy=0;								// konf. kluczy 
//================================================================================


//zmienne pr�dkoci ENC: ==========================================================
volatile float	wFPGA_z,wFPGA_d;	//Predkosci liczone w FPGA zgrubna i dokladna
//================================================================================

float czas, kat1, kat2, delta, speed, pi, KAT ;






