//hardware.h			v1.3 

//************************ Adresy BAZOWE MB ****************************
volatile unsigned *p_CS_FPGA		= (volatile unsigned *)(0x00800000); // szyna danych 32-bit.
volatile unsigned *p_CS_WY_BIN	= (volatile unsigned *)(0x00800019); // szyna danych 32-bit.

volatile unsigned *p_CS_ADC3		= (volatile unsigned *)(0x00900000); // ADC
volatile unsigned *p_CS_7841		= (volatile unsigned *)(0x00A00000); // DAC	
volatile unsigned *p_CS_STYK		= (volatile unsigned *)(0x00B00000); // WE/WY binarne (16bit MSB) 
volatile unsigned *p_CS_ADC1		= (volatile unsigned *)(0x00C00000); // ADC AD7864
volatile unsigned *p_CS_P2			= (volatile unsigned *)(0x00D00000); // n/d
volatile unsigned *p_CS_ADC2		= (volatile unsigned *)(0x00E00000); // ADC MAX1324
volatile unsigned *p_CS_P1			= (volatile unsigned *)(0x00F00000); // n/d
//************************ Adresy BAZOWE MB ****************************

//************************ Adresy rejestr�w FPGA ****************************
volatile unsigned *CS_DELTA_F		= (volatile unsigned *)(0x00800000+0x0F);	//wejscie do delta modulacji 3bit.
volatile unsigned *CS_PWM_F_w		= (volatile unsigned *)(0x00800000+0x10);	//wejscie do PWM faza w
volatile unsigned *CS_PWM_F_v		= (volatile unsigned *)(0x00800000+0x11);	//wejscie do PWM faza v
volatile unsigned *CS_PWM_F_u		= (volatile unsigned *)(0x00800000+0x12);	//wejscie do PWM faza u
volatile unsigned *CS_IFO_F		= (volatile unsigned *)(0x00800000+0x18);	//Odczyt/kasowanie-"0" IFO DC/AC
	
volatile unsigned *CS_LCD			= (volatile unsigned *)(0x00800000+0x20);	
	
volatile unsigned *CS_DELTA_P		= (volatile unsigned *)(0x00800000+0x13);	//wejscie do delta modulacji 3bit.
volatile unsigned *CS_PWM_P_w		= (volatile unsigned *)(0x00800000+0x14);	//wejscie do PWM faza w
volatile unsigned *CS_PWM_P_v		= (volatile unsigned *)(0x00800000+0x15);	//wejscie do PWM faza v
volatile unsigned *CS_PWM_P_u		= (volatile unsigned *)(0x00800000+0x16);	//wejscie do PWM faza u
volatile unsigned *CS_IFO_P		= (volatile unsigned *)(0x00800000+0x17);	//Odczyt/kasowanie-"0" IFO AC/DC	
		
volatile unsigned *CS_WE_BIN		= (volatile unsigned *)(0x00800000+0x1F);	//
volatile unsigned *CS_WY_BIN		= (volatile unsigned *)(0x00800000+0x19);	//wyjcia binarne FPGA (L1..L8)
volatile unsigned *CS_ENC_wd		= (volatile unsigned *)(0x00800000+0x1B);	//przedkosc z ENC (zgrubna)
volatile unsigned *CS_ENC_wz		= (volatile unsigned *)(0x00800000+0x1C);	//przedkosc z ENC (dokladna)
volatile unsigned *CS_ENC_KAT		= (volatile unsigned *)(0x00800000+0x1D);	//sygnal licznika konta z ENC	
//************************ Adresy rejestr�w FPGA ****************************

#define BitSET(x,y) x|=(1<<y)
#define BitCLR(x,y) x&=(~(1<<y))


//PROCEDURY OBS�UGI UKLADOW PERYFERYJNYCH

//AD7841 8ch. 14bit.
void DAC_WR(daneCA &WSK) //wartos�i 
{																								
	*(p_CS_7841+0)=(0x3FFF&(int)(WSK.A*819.1)); 									
	*(p_CS_7841+1)=(0x3FFF&(int)(WSK.B*819.1));									
	*(p_CS_7841+2)=(0x3FFF&(int)(WSK.C*819.1));									
	*(p_CS_7841+3)=(0x3FFF&(int)(WSK.D*819.1));									
	*(p_CS_7841+4)=(0x3FFF&(int)(WSK.E*819.1)); 									
	*(p_CS_7841+5)=(0x3FFF&(int)(WSK.F*819.1));									
	*(p_CS_7841+6)=(0x3FFF&(int)(WSK.G*819.1)); 									
	*(p_CS_7841+7)=(0x3FFF&(int)(WSK.H*819.1));									
}//***********************************************************************

//AD7864 4ch. 12-bit	
void ADC1_RD(daneAC &WSK) //zmienne modyfikowane struktury daneAC przez wskaznik;
{	volatile int ACtmp;														
	ACtmp=(*p_CS_ADC1 & 0x0000FFF0); ACtmp=(ACtmp<<16)>>20;	WSK.CH1=ACtmp;	
	asm("nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;");
	ACtmp=(*p_CS_ADC1 & 0x0000FFF0); ACtmp=(ACtmp<<16)>>20;	WSK.CH2=ACtmp;	
	asm("nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;");
	ACtmp=(*p_CS_ADC1 & 0x0000FFF0); ACtmp=(ACtmp<<16)>>20;	WSK.CH3=ACtmp;	
	asm("nop;nop;nop;nop;nop;nop;nop;nop;nop;nop;");
	ACtmp=(*p_CS_ADC1 & 0x0000FFF0); ACtmp=(ACtmp<<16)>>20;	WSK.CH4=ACtmp;
}//***********************************************************************


//MAX1324 8ch. 14-bit	 
void ADC2_RD(daneAC &WSK) //zmienne modyfikowane struktury daneAC przez wskaznik;
{	 volatile int ACtmp;	
   	ACtmp=(*p_CS_ADC2 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH1=ACtmp;
   	ACtmp=(*p_CS_ADC2 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH2=ACtmp;
   	ACtmp=(*p_CS_ADC2 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH3=ACtmp;
   	ACtmp=(*p_CS_ADC2 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH4=ACtmp;
   	ACtmp=(*p_CS_ADC2 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH5=ACtmp;
   	ACtmp=(*p_CS_ADC2 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH6=ACtmp;
   	ACtmp=(*p_CS_ADC2 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH7=ACtmp;
   	ACtmp=(*p_CS_ADC2 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH8=ACtmp;
}//***********************************************************************
 
//MAX1324
void ADC3_RD(daneAC &WSK)// zmienne modyfikowane struktury daneAC przez wskaznik;
{	volatile int ACtmp;	 	
   	ACtmp=(*p_CS_ADC3 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH1=ACtmp;
   	ACtmp=(*p_CS_ADC3 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH2=ACtmp;
   	ACtmp=(*p_CS_ADC3 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH3=ACtmp;
   	ACtmp=(*p_CS_ADC3 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH4=ACtmp;
   	ACtmp=(*p_CS_ADC3 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH5=ACtmp;
   	ACtmp=(*p_CS_ADC3 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH6=ACtmp;
   	ACtmp=(*p_CS_ADC3 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH7=ACtmp;
   	ACtmp=(*p_CS_ADC3 & 0x0000FFFC); ACtmp=(ACtmp<<16)>>18;	WSK.CH8=ACtmp;
}//***********************************************************************


//sterowanie linijk� LED L1...L8,
void SET_LED(int LED)//LED (od L1 do L8)
{	
	*CS_WY_BIN = LED & 0xFF ;
} 

//odczyt pr�dko�i z FPGA
float RD_wz()
{
	int Wtmp=(*CS_ENC_wz & 0x00007FFF); 
	Wtmp=(Wtmp<<2);
	return (float)Wtmp*0.1;
}

float RD_wd()
{
	int Wtmp=(*CS_ENC_wd & 0x00007FFF); 
	Wtmp=(Wtmp<<2);
	return (float)Wtmp*0.1;
}

//Obs�uga buzzera
void BUZZER (bool Buzz)
{
	if (Buzz) {SRU(HIGH, DPI_PB14_I);}      	//w��cz  buzzer 
	else SRU(LOW, DPI_PB14_I);                //wy��cz buzzer
}

//op�nienie us
void wait_us(long int nops)//dla 350MHz
{
	nops=(nops+nops*17);
 	while(--nops>0)
	{ 
		asm("nop;nop;nop;nop;nop;nop;nop;nop;");
		asm("nop;nop;nop;nop;nop;nop;nop;nop;");
	}
}

//*************************    Obs�uga panelu z wyswietlaczem    *************************

int WY_ST=0x00000000;

//procedura przesy�ania infrmacji do LCD
void LCD_SEND(char DATA, bool Symbol)
{
	int data = (DATA<<2);      //przesuni�cie o dwa bity, bo dwa najm�odsze to RS i E
	if (Symbol) data += 0x01;  //wystawienie bitu na lini podpi�tej do pinu RS decyduje o docelowym obszarze pami�ci LCD
										//maska 0xC0FF0000 zabespiecza linie podpi�t� do gniazda WY_ST przed nadpisaniem (przyciski LED)
										//zeruj�c przy tym tylko te bity bior�ce udzia� w transmisji danych do LCD 	
	WY_ST  =  ((WY_ST & 0xC0FF0000) | (data << 24));
	*p_CS_STYK= WY_ST;	  
										//dodanie 0x02 (0000 0010b) wyzwala zapis do pami�ci mikrokntrolera LCD
	WY_ST =   ((WY_ST & 0xC0FF0000) | ((data|0x02) << 24));
	*p_CS_STYK= WY_ST;	

	WY_ST  =  ((WY_ST & 0xC0FF0000) | (data << 24));
	*p_CS_STYK= WY_ST;	
	wait_us(100);	            //czas  wymagany do poprawnego odczytu danych przez LCD
}

//Procedura konfiguracji wyswietlacza alfanumerycznego
void LCD_INIT(void)
{	
	SRU (LOW, DPI_PB01_I);	 //odblokowanie bufor�w sygna�owych portu WE_ST					
	wait_us(50000);
	
    //3krotne
	LCD_SEND(0x03,false);
	wait_us(10000);
	LCD_SEND(0x03,false);
	LCD_SEND(0x03,false);
	
	//fUNCTION SET 
	LCD_SEND(0x02,false);   //4-BIT INTERFACE	
	LCD_SEND(0x02,false);
	LCD_SEND(0x08,false);   //TWO LINE DISPLAY	
	
	//DISPAY ON/OFF
	LCD_SEND(0x00,false);
	LCD_SEND(0x0C,false);   //SET DISPLAY, NO CURSOR, NO BLINKING CURSOR
	
	//ENTRY MODE SET
	LCD_SEND(0x00,false);
	LCD_SEND(0x06,false);   //CURSOR MOVE DIRECTION, NO DISPLAY SHIFT
}

//czyszczenie wywietlacza	
void LCD_CLR(void)	
{ 
	LCD_SEND(0x00,false);
	LCD_SEND(0x01,false);
	wait_us(1400);	
}

//przejcie na pozycje o t�rej ma si� zacz�c wypisywanie znak�w, 0 - pocz�tek pierwszego wiersza, 40 - pocz�tek drugiego wiersza
void LCD_POZYCJA (char pozycja)
{
	char POZ = ((pozycja & 0x7F)|0x80) ;
	char MSB=0,LSB=0;
	
	MSB=(((POZ)&(0xF0))>>4);		 
	LSB=(POZ)&(0x0F);				 	
	LCD_SEND(MSB,false);
   LCD_SEND(LSB,false);
	
}	
//wyswitlanie pojedynczego znaku
void LCD_PISZ_ZNAK(int ZNAK)		//bajt znaku jest wysy�any w dw�ch blokach po 4 bity
{									 		//pocz�wszy od starszych bit�w
	char MSB=0,LSB=0;	
								     
   MSB=(((ZNAK)&(0xF0))>>4);		//wy�uskanie starszych bit�w przesy�anego znaku
   LSB=(ZNAK)&(0x0F);				//wy�uskanie m�odszych bit�w przesy�anego znaku	
   LCD_SEND(MSB,true);
   LCD_SEND(LSB,true);		
}

//wywietlanie ci�gu znak�w    
void LCD_PISZ_SLOWO (char *text,int dlugosc) //wywietlanie tekstu polega na przes�y�aniu pojedynczych kolejnych znak�w
{										       			//konieczne jest podanie d�ugoci wyswietlanego tekstu
	int	w=0;   
	while (w<dlugosc)
			{LCD_PISZ_ZNAK(text[w]);w++;}			
}

//w��czanie podswietlenia LCD
void LCD_BACKLIGHT(bool light) 
{       
	if (light) WY_ST = (WY_ST | (1 << 30));  	//ustawienie bitu podwietlenia
	else 	  	 WY_ST = (WY_ST & 0x3FFF0000);	//zerowanir bitu podswietlenia
	*p_CS_STYK = WY_ST;	                     //wystawienie na szyn� danych
}

//Odczyt przyciskow konsoli
int BUTTONS_RD()
{  			
	volatile int Buttons = (*p_CS_STYK);		//odczytanie wartoci z lini danych
	Buttons = ((Buttons>> 16) & (0x000000FF));//przesuni�cie w stron� m�odszych bit�w	
	return Buttons;			
}

//sterowanie diodami przycisk�w
void BUTTONS_LED(int LED)
{
	int diody=(LED & 0xFF);						   	//opgraniczenie d�ugosci do 8 bit�w
	WY_ST = ((WY_ST & 0xFF000000)|(diody << 16));//wystawienie na odpowiednie linie
	*p_CS_STYK = WY_ST;	
}
//****************************************************************************************
